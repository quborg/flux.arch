Gofa frontend.
--------------
React Flux architecture

To run this application, you should do following:
```
npm install
```
```
gulp
```
```
npm start
```
Navigate to http://localhost:3000

You can change port in package.json at line 7

For Developers, who want use JavaScript Code Style Checker
----------------------------------------------------------

```
npm install jscs -g
```
Go to root of application and run following:
```
jscs .
```
