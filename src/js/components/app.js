var React = require('react');
var Catalog = require('./catalog/appCatalog');
var Cart = require('./cart/appCart');
var Router = require('react-router-component');
var CatalogDetail = require('./product/appCatalogDetail');
var Template = require('./appTemplate');
var Locations = Router.Locations;
var Location = Router.Location;

var App = React.createClass({
  render: function() {
    return (
      <Template>
        <Locations>
          <Location path="/" handler={Catalog} />
          <Location path="/cart" handler={Cart} />
          <Location path="/item/:item" handler={CatalogDetail} />
        </Locations>
      </Template>
    )
  }
});

module.exports = App;
