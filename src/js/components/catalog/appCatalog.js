var React = require('react');
var AppStore = require('../../stores/appStore');
var AddToCart = require('./appAddToCart');
var StoreWatchMixin = require('../../mixins/StoreWatchMixin');
var CatalogItem = require('../catalog/appCatalogItem');

function getCatalog() {
  return {items: AppStore.getCatalog()};
}

var Catalog = React.createClass({
  mixins: [StoreWatchMixin(getCatalog)],
  render: function() {
    var items = this.state.items.map(function(item){
      return <CatalogItem key={item.id} item={item} />
    });
    return (
      <div className="row">
        {items}
      </div>
    );
  }
});

module.exports = Catalog;
