var React = require('react');
var AppStore = require('../stores/appStore');

var StoreWatchMixin = function(cb) {
  return {
    getInitialState: function() {
      return cb(this);
    },
    componentWillMount: function() {
      AppStore.addChangeListener(this._onChange);
    },
    componentWillUnmount: function() {
      AppStore.addChangeListener(this._onChange);
    },
    _onChange: function() {
      if (this.isMounted()) {
        this.setState(cb(this));
      }
    },
  };
};

module.exports = StoreWatchMixin;
